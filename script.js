document.getElementById("countButton").onclick = function() {
    let typedText = document.getElementById("textInput").value;
    typedText = typedText.toLowerCase();
    typedText = typedText.replace(/[^a-z'\s]+/g, "");
    const letterCounts = {};
    const word = {};
    const words = typedText.split(" ");


    for (let i = 0; i < typedText.length; i++) {
        currentLetter = typedText[i];

        if(letterCounts[currentLetter] === undefined) {
            letterCounts[currentLetter] = 1;
         } else {
            letterCounts[currentLetter]+= 1;
         }
     }

     for (let letter in letterCounts) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ");
        span.appendChild(textContent);
        document.getElementById("lettersDiv").appendChild(span);
     }


     for (let i = 0; i < words.length; i++) {
        currentWord = words[i];

        if(word[currentWord] === undefined) {
            word[currentWord] = 1;
         } else {
            word[currentWord]+= 1;
         }
     }
     for (let w in word) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + w + "\": " + word[w] + ", ");
        span.appendChild(textContent);
        document.getElementById("wordsDiv").appendChild(span);
     }
     console.log(words)
 }

 document.getElementById("clear").onclick = function() {
    document.getElementById("textInput").value = "";
    let element = document.getElementById('lettersDiv');
    while (element.firstChild) {
    element.removeChild(element.firstChild);
    }
    let element2 = document.getElementById('wordsDiv');
    while (element2.firstChild) {
    element2.removeChild(element2.firstChild);
    }
}
